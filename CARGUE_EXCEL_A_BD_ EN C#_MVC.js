
// Se creación de los métodos para el bullCopy

// Instancia de la conexión a la base de datos.
private static ContextModel db = new ContextModel();

// Método para insertar los datos.
public bool InsertarUsuariosAplicativos(string path)
{

    try
    {
        // Pasamos el nombre de la tabla que se va a truncar.
        TruncarTablaTemporal("TemporalAsignacionAplicativosUsuario");

        DataTable dtUsuarios = CargarExcel(path);

        BulkAsignacionAplicativosUsuario(dtUsuarios);

        return true;

    }
    catch (Exception ex)
    {
        throw new Exception(ex.Message);
    }

}

public DataTable CargarExcel(string cadenaConexion)
{

    try
    {
        var strconn = ("Provider=Microsoft.ACE.OLEDB.12.0;" +
        ("Data Source=" + (cadenaConexion + ";Extended Properties=\"Excel 12.0;HDR=YES\"")));

        DataTable dataTable = new DataTable();


        OleDbConnection mconn = new OleDbConnection(strconn);
        mconn.Open();
        DataTable dtSchema = mconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

        if ((null != dtSchema) && (dtSchema.Rows.Count > 0))
        {
            string firstSheetName1 = dtSchema.Rows[2]["TABLE_NAME"].ToString();
            string firstSheetName = "Cargue$";
            new OleDbDataAdapter("SELECT * FROM [" + firstSheetName.Trim() + "]", mconn).Fill(dataTable);
        }
        mconn.Close();

        return dataTable;
    }
    catch (Exception ex)
    {
        throw new Exception(ex.Message);
    }


}


public void BulkAsignacionAplicativosUsuario(DataTable dtUsuarios)
{


    List<ErroresFormatoVM> errores = new List<ErroresFormatoVM>();

    using (SqlBulkCopy bulkcopy = new SqlBulkCopy(db.Database.Connection.ConnectionString))
    {
        bulkcopy.DestinationTableName = "TemporalAsignacionAplicativosUsuario";
        bulkcopy.ColumnMappings.Add("Cedula", "Documento");
        bulkcopy.ColumnMappings.Add("Nombre_Aplicativo", "NombreAplicativo");
        bulkcopy.ColumnMappings.Add("Usuario_Aplicativo", "UsuarioAplicativo");
        bulkcopy.ColumnMappings.Add("Clave_Aplicativo", "ClaveAplicativo");

        try
        {
            bulkcopy.WriteToServer(dtUsuarios);
            errores = ValidarCamposVaciosBS.ValidarCamposVaciosUsuariosAPlicativos("P_ValidarCamposVaciosAsignacionAplicativos");


            if (errores.Count > 0)
            {
                ExportExcel(errores);
                throw new Exception(Configuraciones.MENSAJE_ERROR_FORMATO_ASIGNACION);
            }
            else
            {
                InsertarAplicativosUsuario();
            }
                                

        }
        catch (Exception ex)
        {
            if (ex.HResult == -2146233079)
            {
                throw new Exception(Configuraciones.MENSAJE_ERROR_COLUMNAS);
            }
            else
            {
                throw new Exception(ex.Message);
            }

        }
    }
}

public void InsertarAplicativosUsuario()
{
    try
    {
        SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

        SqlCommand comandoInsert = new SqlCommand("SP_CrearActualizarUsuarios", connection);

        comandoInsert.CommandType = CommandType.StoredProcedure;

        // Add the input parameter and set its properties.
        SqlParameter parameter = new SqlParameter();

        comandoInsert.Parameters.Clear();

        connection.Open();
        SqlDataReader reader = comandoInsert.ExecuteReader();

        connection.Close();

    }
    catch (Exception ex)
    {
        throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
    }
}


public void ExportExcel(List<ErroresFormatoVM> erroresFormatoCargue)
{

    var archivoErroresCargue = ValidarArchivoExistenteCarpeta();

    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
    ExcelPackage pck = new ExcelPackage(archivoErroresCargue);
    try
    {
        ExcelWorksheet ws = pck.Workbook.Worksheets.Add("ErroresCargue" + DateTime.Now);

        ws.Cells["A1"].Value = "Id";
        ws.Cells["B1"].Value = "TipoError";

        int loop = 1;

        foreach (var item in erroresFormatoCargue)
        {
            loop++;

            ws.Cells["A" + loop].Value = item.Identificacion;
            ws.Cells["B" + loop].Value = item.TipoError;

        }
        ws.Cells["A:AZ"].AutoFitColumns();
        pck.Save();
    }
    catch (Exception ex)
    {
        throw new Exception(ex.Message);
    }

}

public FileInfo ValidarArchivoExistenteCarpeta()
{
    FileInfo newFile = new FileInfo(HttpContext.Current.Server.MapPath("~/ErroresFormato/ErroresFormatoAplicativosUsuario.xlsx"));

    if (newFile.Exists)
    {
        newFile.Delete();
    }
    return newFile;
}


// Método para truncar la tabla temporal
public void TruncarTablaTemporal(string Tabla)
{

    try
    {
        SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString);

        SqlCommand comandoInsert = new SqlCommand("TRUNCATE TABLE "+ Tabla, connection);

        comandoInsert.CommandType = CommandType.Text;

        // Add the input parameter and set its properties.
        SqlParameter parameter = new SqlParameter();

        comandoInsert.Parameters.Clear();

        connection.Open();
        SqlDataReader reader = comandoInsert.ExecuteReader();

        connection.Close();

    }
    catch (Exception ex)
    {
        throw new Exception("Error al ejecutar procedimiento almacenado. \n" + ex.Message.ToString());
    }
}


public JsonResult CargarArchivoUsuarios()
{
    if (Request.Files.Count > 0)
    {
        try
        {
            HttpFileCollectionBase files = Request.Files;

            HttpPostedFileBase file = files[0];
            string fileName = file.FileName;

            EliminarArchivosResources();

            string path = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario"),
                          Path.GetFileName(file.FileName));
            file.SaveAs(path);

            var dt = cargueUsuariosAplicativosBS.InsertarUsuariosAplicativos(path);

            return Json(new { status = 201, message = "El archivo se ha cargado correctamente!" });


        }

        catch (Exception ex)
        {
            return Json(new { status = 504, message = ex.Message });
        }
    }

    return Json("Por favor seleccione el archivo");
}


public void EliminarArchivosResources()
{
    try
    {
        var pathResources = Path.Combine(Server.MapPath("~/Resources/CargueAppUsuario").ToString());

        DirectoryInfo di = new DirectoryInfo(pathResources);

        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();

        }
    }
    catch (Exception ex)            {
       
        throw new Exception();
    }
}




// Método que dirige a la vista de carga del archivo.

public ActionResult Index()
{
    return View();
}


// Vista en razor para la carga del archivo y descargue del formato.


@{
    ViewBag.Title = "Index";
}


<div class="box box-danger">
    <div class="box-header with-border text-center">
        <h2 class="box-title">Cargue Contraseñas y Usuarios</h2>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">

                    <a href="~/Formato/CargueAppUsuario/Formato_Administracion_Usuarios.xlsx" download class="btn btn-primary">Descargar Formato</a>


                </div>
            </div>
        </div>
    </div>
</div>



<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Cargar contraseñas aplicativos</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal">
                    @if (TempData["Message"] != null)
                    {
                        <div class="alert alert-info  alert-dismissible" role="alert">
                            @TempData["Message"]
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    }

                    <div class="form-group">

                        <div class="col-md-6">
                            <input type="file" id="fileUsuario" class="form-control" />
                        </div>

                        <div class="col-md-6" style="margin-top:20px">
                            <input type="button" id="cargarUsuarios" value="Cargar" class="btn btn-success" />
                            <a href="@Url.Action("Index", "AsignacionAplicativos")" class="btn btn-dark">Regresar</a>
                        </div>

                    </div>
                </div>

                <div id='loading' style="display:none"><img src='~/Images/loader.gif' /><br />Un momento, por favor...</div>

            </div>
            

        </div>
        <hr />
        <div class="row">
            <div class="col-md-12">
                <div class="form-horizontal" style="display:none" id="mostrarErrores">
                    <div class="form-group">
                        <a value="Descargar Errores" class="btn btn-warning" href="~/ErroresFormato/ErroresFormatoAplicativosUsuario.xlsx" download>Descargar Formato Errores </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@section Scripts {
    <script src="~/Scripts/app/cargarArchivo.js"></script>
}


// javascript para la carga del archivo.
$('body').on('click', '#cargarUsuarios', function () {

    if (window.FormData == undefined)
        alert("Error: FormData is undefined");

    else {
        var fileUpload = $("#fileUsuario").get(0);
        var files = fileUpload.files;

        if (files.length === 0) {
            swal({
                position: 'top-right',
                type: 'error',
                title: "por favor seleccionar el archivo.",
                showConfirmButton: false,
                timer: 1800
            });
        } else {

            $('#loading').css('display', 'block');
            var fileData = new FormData();

            fileData.append(files[0].name, files[0]);

            $.ajax({
                url: '/CargueUsuarios/CargarArchivoUsuarios',
                type: 'post',
                datatype: 'json',
                contentType: false,
                processData: false,
                async: true,
                data: fileData,
                success: function (response) {
                    if (response.status === 201) {

                        swal({
                            position: 'top-right',
                            type: 'success',
                            title: response.message,
                            showConfsirmButton: false,
                            timer: 3000
                        });

                        $("#mostrarErrores").css("display", "none");
                        $('#loading').css('display', 'none');


                        limpiarFileInput("fileUsuario");

                    } else {
                        swal({
                            position: 'top-right',
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        if (response.message.includes('Error Formato')) {
                            $("#mostrarErrores").css("display", "block");
                        }
                        limpiarFileInput("fileUsuario");
                        $('#loading').css('display', 'none');

                    }
                }
            });
        }
    }

});


function limpiarFileInput(idFileInput) {
    document.getElementById(idFileInput).value = "";
}