﻿namespace ClaroGrandesSuperficies.Viewmodel
{
    public class UserActiveDirectory
    {
        /// <summary>
        /// Obtiene y/o establece el user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Obtiene y/o establece el display name.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Obtiene y/o establece el company.
        /// </summary>
        public string Company { get; set; }

        /// <summary>
        /// Obtiene y/o establece el deparment.
        /// </summary>
        public string Deparment { get; set; }

        /// <summary>
        /// Obtiene y/o establece el job title.
        /// </summary>
        public string JobTitle { get; set; }

        /// <summary>
        /// Obtiene y/o establece el email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Obtiene y/o establece el phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Obtiene y/o establece el mobile.
        /// </summary>
        public string Mobile { get; set; }
    }
}