﻿using ClaroGrandesSuperficies.Viewmodel;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ClaroGrandesSuperficies.BusinessLogic
{
    public class ActiveDirectory
    {
        #region Métodos Asíncronos.

        /// <summary>
        /// Método asíncrono que permite obtener un usuario determinado del active directory por dominio, usuario y contraseña.
        /// </summary>
        /// <param name="activeDirectoryFilterVO"></param>
        /// <returns></returns>
        public async Task<UserActiveDirectory> GetUserActiveDirectoryAsync(ActiveDirectoryFilterVO activeDirectoryFilterVO)
        {
            // Definición de variables.
            UserActiveDirectory user = null;
            string domainPath = $"LDAP://{ activeDirectoryFilterVO.Domain }";
            DirectoryEntry adSearchRoot = new DirectoryEntry(domainPath, activeDirectoryFilterVO.UserName, activeDirectoryFilterVO.Password);
            DirectorySearcher adSearcher = new DirectorySearcher(adSearchRoot)
            {
                // Se busca por usuario.
                Filter = $"samaccountname={ activeDirectoryFilterVO.UserName }"
            };

            // Se agregan las propiedades para el modelo.
            adSearcher.PropertiesToLoad.Add("samaccountname");
            adSearcher.PropertiesToLoad.Add("title");
            adSearcher.PropertiesToLoad.Add("mail");
            adSearcher.PropertiesToLoad.Add("usergroup");
            adSearcher.PropertiesToLoad.Add("company");
            adSearcher.PropertiesToLoad.Add("department");
            adSearcher.PropertiesToLoad.Add("telephoneNumber");
            adSearcher.PropertiesToLoad.Add("mobile");
            adSearcher.PropertiesToLoad.Add("displayname");
            SearchResult result;

            // Se aplica el filtro.
            result = adSearcher.FindOne();

            // Se valida el resultado para crear el objeto.
            if (result != null)
            {
                if (result.Properties.Contains("samaccountname"))
                {
                    user = new UserActiveDirectory
                    {
                        UserName = result.Properties["samaccountname"][0].ToString()
                    };

                    if (result.Properties.Contains("displayname"))
                    {
                        user.DisplayName = result.Properties["displayname"][0].ToString();
                    }

                    if (result.Properties.Contains("mail"))
                    {
                        user.Email = result.Properties["mail"][0].ToString();
                    }

                    if (result.Properties.Contains("company"))
                    {
                        user.Company = result.Properties["company"][0].ToString();
                    }

                    if (result.Properties.Contains("title"))
                    {
                        user.JobTitle = result.Properties["title"][0].ToString();
                    }

                    if (result.Properties.Contains("department"))
                    {
                        user.Deparment = result.Properties["department"][0].ToString();
                    }

                    if (result.Properties.Contains("telephoneNumber"))
                    {
                        user.Phone = result.Properties["telephoneNumber"][0].ToString();
                    }

                    if (result.Properties.Contains("mobile"))
                    {
                        user.Mobile = result.Properties["mobile"][0].ToString();
                    }
                }
            }

            // Se libera la memoria.
            adSearcher.Dispose();
            adSearchRoot.Dispose();

            // Se retorna el resultado requerido.
            return await Task.Run(() => user);
        }

        /// <summary>
        /// Método asíncrono que permite obtener la lista de dominios.
        /// </summary>
        /// <returns></returns>
        public async Task<IList<DomainActiveDirectory>> GetDomainsActiveDirectoryAsync()
        {
            // Definición de variables.
            var listDomains = new List<DomainActiveDirectory>();

            // Se obtiene la lista de dominios.
            listDomains.AddRange(Forest.GetCurrentForest().Domains.Cast<Domain>().
                                                            Select(x =>
                                                            new DomainActiveDirectory
                                                            {
                                                                Domain = x.Name
                                                            }).ToList());

            // Se retorna el resultado requerido.
            return await Task.Run(() => listDomains);
        }

        #endregion
    }
}